A fair chunk of patches for LineageOS17/17.1 on the OG Pixel XL.
- battery_ui-{1,2,3}.patch - frameworks/base
  - Applies patches to the SystemUI to enable the battery icon features
    that were in Lineage OS 16.
- build-ctsprofile.patches
  - Patches for ctsprofile to pass
- buildinfo.patch - build/tools
  - Applies build.prop tweaks
- cmdline.patch - kernel/google/marlin
  - Patches the command line to pass ctsProfile  
- device-marlin.patch - device/google/marlin
  - Patches in MTG and gavapps to the builds.
- \*Overlay.patch
  - Creates the overlay for custom theming
- Remove-default-apps-{1,2}.patch
  - Removes the default apps from fresh builds
- selinux.patch - external/selinux
  - Patches selinux to always pass basicintegrity, even when
    in permissive mode.
- version.patch - build/code
  - Patches in version string to pass ctsProfile.
- wellbeing.patch - vendor/lineage
  - Applies patches to make Digital Wellbeing work,
    and one random patch to show battery percentage text.
